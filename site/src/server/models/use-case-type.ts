export enum UseCaseType {
    Extension,
    Feature,
    Prototype,
    Api,
    Event
}
