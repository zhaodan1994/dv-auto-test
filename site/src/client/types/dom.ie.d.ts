interface HTMLScriptElement {
    readyState: string;
    onreadystatechange: () => void;
}

interface HTMLDivElement {
    children: any
}
