export class Option {
    fullModel: object;
    snippet: object;
    data: string;
    geojson: string;

    constructor(fullModel: object, snippet: object, data: string, geojson: string) {
        this.fullModel = fullModel;
        this.snippet = snippet;
        this.data = this.data;
        this.geojson = this.geojson;
    }

}