import { UseCaseModel } from './use-case.model';

export class UseCaseViewModel extends UseCaseModel {
  readonly keys: string[] = [];
}