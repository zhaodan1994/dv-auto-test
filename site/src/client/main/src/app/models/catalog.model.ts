import { Model } from './model';
export class CatalogModel extends Model {
    readonly children: Model[] = [];
}