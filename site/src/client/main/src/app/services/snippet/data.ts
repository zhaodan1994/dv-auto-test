export const enumType = {
    RenderMethod: [
      'Canvas',
      'SVG'
    ],
    PlotType: [
      'Bar',
      'Line',
      'Area',
      'Scatter',
      'TreeMap',
      'Text',
      'Candlestick',
      'HighLowOpenClose',
      'Map'
    ],
    ExcludeNulls: [
      'true',
      'false'
    ],
    AxisMode: [
      'Cartesian',
      'Radial',
      'Polygonal'
    ],
    ClippingMode: [
      'None',
      'Fit',
      'Clip'
    ],
    LineAspect: [
      'Default',
      'Spline',
      'StepLeft',
      'StepRight',
      'StepCenter'
    ],
    ShowNulls: [
      'Gaps',
      'Zeros',
      'Connected'
    ],
    Symbols: [
      'true',
      'false'
    ],
    SwapAxes: [
      'true',
      'false'
    ],
    LinePosition: [
      'Auto',
      'Center'
    ],
    AnimationMode: [
      'All',
      'Point',
      'Group'
    ],
    AnimationEasing: [
      'Linear',
      'Swing',
      'EaseInQuad',
      'EaseOutQuad',
      'EaseInOutQuad',
      'EaseInCubic',
      'EaseOutCubic',
      'EaseInOutCubic',
      'EaseInQuart',
      'EaseOutQuart',
      'EaseInOutQuart',
      'EaseInQuint',
      'EaseOutQuint',
      'EaseInOutQuint',
      'EaseInSine',
      'EaseOutSine',
      'EaseInOutSine',
      'EaseInExpo',
      'EaseOutExpo',
      'EaseInOutExpo',
      'EaseInCirc',
      'EaseOutCirc',
      'EaseInOutCirc',
      'EaseInBack',
      'EaseOutBack',
      'EaseInOutBack',
      'EaseInBounce',
      'EaseOutBounce',
      'EaseInOutBounce',
      'EaseInElastic',
      'EaseOutElastic',
      'EaseInOutElastic'
    ],
    Palette: [
      'Standard',
      'Cocoa',
      'Coral',
      'Dark',
      'HighContrast',
      'Light',
      'Midnight',
      'Modern',
      'Organic',
      'Slate',
      'Zen',
      'Cyborg',
      'Superhero',
      'Flatly',
      'Darkly',
      'Cerulan',
      'Office',
      'Office2010',
      'Grayscale',
      'BlueWarm',
      'Blue',
      'Blue2',
      'BlueGreen',
      'Green',
      'GreenYellow',
      'Yellow',
      'YellowOrange',
      'Orange',
      'OrangeRed',
      'RedOrange',
      'Red',
      'RedViolet',
      'Violet',
      'Violet2',
      'Median',
      'Paper',
      'Marquee',
      'Slipstream',
      'Aspect'
    ],
    Orientation: [
      'Horizontal',
      'Vertical'
    ],
    Position: [
      'Front',
      'Back'
    ],
    SelectionMode: [
      'None',
      'Point',
      'Points',
      'Group',
      'Color',
      'Shape',
      'Size',
      'LegendSingle',
      'LegendMultiple',
      'Category',
      'Custom',
      'Trellis'
    ]
  };