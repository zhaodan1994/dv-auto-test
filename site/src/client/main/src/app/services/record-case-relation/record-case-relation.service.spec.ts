import { TestBed } from '@angular/core/testing';

import { RecordCaseRelationService } from './record-case-relation.service';

describe('RecordCaseRelationService', () => {
  let service: RecordCaseRelationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecordCaseRelationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
